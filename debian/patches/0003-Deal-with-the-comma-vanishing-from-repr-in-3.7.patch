From: James Valleroy <jvalleroy@mailbox.org>
Date: Tue, 27 Nov 2018 20:18:49 -0500
Subject: Deal with the comma vanishing from repr in 3.7

Based on https://github.com/Simplistix/testfixtures/commit/6e8807543b804946aba58e2c9e92f5bdc3656a57
---
 docs/exceptions.txt                     |  2 +-
 docs/logging.txt                        |  2 +-
 docs/warnings.txt                       |  6 +--
 testfixtures/compat.py                  |  5 +++
 testfixtures/tests/test_compare.py      | 38 +++++++++++++------
 testfixtures/tests/test_should_raise.py | 65 ++++++++++++++++++++++++---------
 testfixtures/tests/test_shouldwarn.py   | 13 +++++--
 7 files changed, 92 insertions(+), 39 deletions(-)

diff --git a/docs/exceptions.txt b/docs/exceptions.txt
index 67d7e36..eeadc0b 100644
--- a/docs/exceptions.txt
+++ b/docs/exceptions.txt
@@ -38,7 +38,7 @@ causing the tests in which it occurs to fail:
 ...     the_thrower()
 Traceback (most recent call last):
 ...
-AssertionError: ValueError('Not good!',) raised, ValueError('Is good!',) expected
+AssertionError: ValueError('Not good!'...) raised, ValueError('Is good!'...) expected
 
 If you're not concerned about anything more than the type of the
 exception that's raised, you can check as follows:
diff --git a/docs/logging.txt b/docs/logging.txt
index e5d064d..e52b4b8 100644
--- a/docs/logging.txt
+++ b/docs/logging.txt
@@ -227,7 +227,7 @@ A common case of this is where you want to check that exception
 information was logged for certain messages:
 
 >>> print(l.records[-1].exc_info)
-(<... '...RuntimeError'>, RuntimeError('No code to run!',), <traceback object at ...>)
+(<... '...RuntimeError'>, RuntimeError('No code to run!'...), <traceback object at ...>)
 
 If you're working in a unit test, the following code may be more
 appropriate: 
diff --git a/docs/warnings.txt b/docs/warnings.txt
index 2642fa8..147b3b9 100644
--- a/docs/warnings.txt
+++ b/docs/warnings.txt
@@ -39,7 +39,7 @@ expected:
   </C>]
 <BLANKLINE>
 actual:
-[UserWarning("sorry dave, I can't let you do that",)]
+[UserWarning("sorry dave, I can't let you do that"...)]
 
 You can check multiple warnings in a particular piece of code:
 
@@ -61,7 +61,7 @@ them into a list as follows:
 >>> len(captured)
 1
 >>> captured[0].message
-DeprecationWarning('foo',)
+DeprecationWarning('foo'...)
 >>> captured[0].lineno
 42
 
@@ -87,4 +87,4 @@ expected:
 []
 <BLANKLINE>
 actual:
-[UserWarning('woah dude',)]
+[UserWarning('woah dude'...)]
diff --git a/testfixtures/compat.py b/testfixtures/compat.py
index fe4a632..48805b0 100644
--- a/testfixtures/compat.py
+++ b/testfixtures/compat.py
@@ -1,6 +1,11 @@
 # compatibility module for different python versions
 import sys
 
+PY_VERSION = sys.version_info[:2]
+
+PY_37_PLUS = PY_VERSION >= (3, 7)
+
+
 if sys.version_info[:2] > (3, 0):
 
     PY2 = False
diff --git a/testfixtures/tests/test_compare.py b/testfixtures/tests/test_compare.py
index 8e390c7..f525a14 100644
--- a/testfixtures/tests/test_compare.py
+++ b/testfixtures/tests/test_compare.py
@@ -15,7 +15,8 @@ from testfixtures import (
     )
 from testfixtures.compat import (
     class_type_name, exception_module, PY3, xrange,
-    BytesLiteral, UnicodeLiteral
+    BytesLiteral, UnicodeLiteral,
+    PY_37_PLUS
     )
 from testfixtures.comparison import compare_sequence
 from unittest import TestCase
@@ -152,10 +153,16 @@ class TestCompare(CompareHelper, TestCase):
     def test_exception_different_object(self):
         e1 = ValueError('some message')
         e2 = ValueError('some message')
-        self.check_raises(
-            e1, e2,
-            "ValueError('some message',) != ValueError('some message',)"
-            )
+        if PY_37_PLUS:
+            self.check_raises(
+                e1, e2,
+                "ValueError('some message') != ValueError('some message')"
+                )
+        else:
+            self.check_raises(
+                e1, e2,
+                "ValueError('some message',) != ValueError('some message',)"
+                )
 
     def test_exception_different_object_c_wrapper(self):
         e1 = ValueError('some message')
@@ -165,10 +172,16 @@ class TestCompare(CompareHelper, TestCase):
     def test_exception_diff(self):
         e1 = ValueError('some message')
         e2 = ValueError('some other message')
-        self.check_raises(
-            e1, e2,
-            "ValueError('some message',) != ValueError('some other message',)"
-            )
+        if PY_37_PLUS:
+            self.check_raises(
+                e1, e2,
+                "ValueError('some message') != ValueError('some other message')"
+                )
+        else:
+            self.check_raises(
+                e1, e2,
+                "ValueError('some message',) != ValueError('some other message',)"
+                )
 
     def test_exception_diff_c_wrapper(self):
         e1 = ValueError('some message')
@@ -176,12 +189,13 @@ class TestCompare(CompareHelper, TestCase):
         self.check_raises(
             C(e1), e2,
             ("\n"
-             "  <C(failed):{0}.ValueError>\n"
+             "  <C(failed):{}.ValueError>\n"
              "  args:('some message',) != ('some other message',)\n"
              "  </C>"
              " != "
-             "ValueError('some other message',)"
-             ).format(exception_module))
+             "ValueError('some other message'{})"
+             ).format(exception_module,
+                      '' if PY_37_PLUS else ','))
 
     def test_sequence_long(self):
         self.check_raises(
diff --git a/testfixtures/tests/test_should_raise.py b/testfixtures/tests/test_should_raise.py
index 4f860e9..fc548a7 100644
--- a/testfixtures/tests/test_should_raise.py
+++ b/testfixtures/tests/test_should_raise.py
@@ -6,6 +6,7 @@ from unittest import TestCase
 
 from .compat import py_33_plus
 from .compat import py_36_plus
+from ..compat import PY_37_PLUS
 
 
 class TestShouldRaise(TestCase):
@@ -34,11 +35,18 @@ class TestShouldRaise(TestCase):
         try:
             should_raise(ValueError('foo'))(to_test)()
         except AssertionError as e:
-            self.assertEqual(
-                e,
-                C(AssertionError(
-                    "ValueError('bar',) raised, ValueError('foo',) expected"
-                )))
+            if PY_37_PLUS:
+                self.assertEqual(
+                    e,
+                    C(AssertionError(
+                        "ValueError('bar') raised, ValueError('foo') expected"
+                    )))
+            else:
+                self.assertEqual(
+                    e,
+                    C(AssertionError(
+                        "ValueError('bar',) raised, ValueError('foo',) expected"
+                    )))
         else:
             self.fail('No exception raised!')
 
@@ -146,11 +154,18 @@ class TestShouldRaise(TestCase):
             with ShouldRaise(ValueError('foo')):
                 raise ValueError('bar')
         except AssertionError as e:
-            self.assertEqual(
-                e,
-                C(AssertionError(
-                    "ValueError('bar',) raised, ValueError('foo',) expected"
-                )))
+            if PY_37_PLUS:
+                self.assertEqual(
+                    e,
+                    C(AssertionError(
+                        "ValueError('bar') raised, ValueError('foo') expected"
+                    )))
+            else:
+                self.assertEqual(
+                    e,
+                    C(AssertionError(
+                        "ValueError('bar',) raised, ValueError('foo',) expected"
+                    )))
         else:
             self.fail('No exception raised!')
 
@@ -163,10 +178,16 @@ class TestShouldRaise(TestCase):
             with ShouldRaise(ValueError('foo')):
                 pass
         except AssertionError as e:
-            self.assertEqual(
-                e,
-                C(AssertionError("None raised, ValueError('foo',) expected"))
-                )
+            if PY_37_PLUS:
+                self.assertEqual(
+                    e,
+                    C(AssertionError("None raised, ValueError('foo') expected"))
+                    )
+            else:
+                self.assertEqual(
+                    e,
+                    C(AssertionError("None raised, ValueError('foo',) expected"))
+                    )
         else:
             self.fail('No exception raised!')
 
@@ -212,7 +233,10 @@ class TestShouldRaise(TestCase):
             raise FileTypeError('X')
 
     def test_assert_keyerror_raised(self):
-        expected = "KeyError('foo',) raised, AttributeError('foo',) expected"
+        if PY_37_PLUS:
+            expected = "KeyError('foo') raised, AttributeError('foo') expected"
+        else:
+            expected = "KeyError('foo',) raised, AttributeError('foo',) expected"
 
         class Dodgy(dict):
             def __getattr__(self, name):
@@ -259,9 +283,14 @@ class TestShouldRaise(TestCase):
             with ShouldRaise(unless=True):
                 raise AttributeError('foo')
         except AssertionError as e:
-            self.assertEqual(e, C(AssertionError(
-                "AttributeError('foo',) raised, no exception expected"
-                )))
+            if PY_37_PLUS:
+                self.assertEqual(e, C(AssertionError(
+                    "AttributeError('foo') raised, no exception expected"
+                    )))
+            else:
+                self.assertEqual(e, C(AssertionError(
+                    "AttributeError('foo',) raised, no exception expected"
+                    )))
         else:
             self.fail('No exception raised!')
 
diff --git a/testfixtures/tests/test_shouldwarn.py b/testfixtures/tests/test_shouldwarn.py
index 1e9b49c..230da76 100644
--- a/testfixtures/tests/test_shouldwarn.py
+++ b/testfixtures/tests/test_shouldwarn.py
@@ -6,13 +6,18 @@ from testfixtures import (
     ShouldWarn, compare, ShouldRaise, ShouldNotWarn,
     Comparison as C
 )
-from testfixtures.compat import PY3
+from testfixtures.compat import PY3, PY_37_PLUS
 
 if PY3:
     warn_module = 'builtins'
 else:
     warn_module = 'exceptions'
 
+if PY_37_PLUS:
+    comma = ''
+else:
+    comma = ','
+
 from .compat import py_36_plus
 
 
@@ -29,7 +34,7 @@ class ShouldWarnTests(TestCase):
             "sequence not as expected:\n\n"
             "same:\n[]\n\n"
             "expected:\n[]\n\n"
-            "actual:\n[UserWarning('foo',)]"
+            "actual:\n[UserWarning('foo'"+comma+")]"
         )):
             with warnings.catch_warnings(record=True) as backstop:
                 with ShouldNotWarn():
@@ -76,7 +81,7 @@ class ShouldWarnTests(TestCase):
             "same:\n[]\n\n"
             "expected:\n"
             "[<C(failed):"+warn_module+".DeprecationWarning>wrong type</C>]\n\n"
-            "actual:\n[UserWarning('foo',)]"
+            "actual:\n[UserWarning('foo'"+comma+")]"
         )):
             with ShouldWarn(DeprecationWarning):
                 warnings.warn('foo')
@@ -95,7 +100,7 @@ class ShouldWarnTests(TestCase):
             "  <C(failed):"+warn_module+".DeprecationWarning>\n"
             "  args:('bar',) != ('foo',)"
             "\n  </C>]\n\n"
-            "actual:\n[DeprecationWarning('foo',)]"
+            "actual:\n[DeprecationWarning('foo'"+comma+")]"
         )):
             with ShouldWarn(DeprecationWarning('bar')):
                 warnings.warn_explicit(
